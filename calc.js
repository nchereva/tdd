Calculator = function() {

    this.sum = function(a, b) {
        return a + b;
    };

    this.div = function(a, b) {
        if (b !== 0) {
            return a / b;
        } else {
            return Infinity;
        }
    };
};

VolumeCalculator = function() {
    this.calculateLiquidWeight = function(width, height, depth, wallThickness, level) {
        var h = wallThickness;
        return {
            weight: ((width - h) * (level) * (depth - h) * 1000),
            volume: ((width - h) * (level) * (depth - h))
        };
    };
};
