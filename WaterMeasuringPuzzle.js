var Puzzle = function() {
    this.tubes = {
        threeGallon: new Tube(3),
        fourGallon: new Tube(4)
    };
    this.limit = 0;
    this.checkLimit = function() {
        for (var tube in this.tubes) {
            if (this.tubes[tube].actualVolume === this.limit) {
                return true;
            }
            return false;
        }
    };
    this.mix = function(from, to) {

        var result = (this.tubes[from].actualVolume + this.tubes[to].actualVolume);

        var rest = (result > this.tubes[to].volume) ? (result % this.tubes[to].volume) : 0;
        this.tubes[to].actualVolume = result - rest;
        this.tubes[from].actualVolume = rest;
    };
};

var Tube = function(volume) {
    this.volume = volume;
    this.actualVolume = 0;
    this.fill = function() {
        this.actualVolume = this.volume;
        return this.actualVolume;
    };
    this.isEmpty = function() {
        return (this.actualVolume === 0);
    };
    this.pour = function() {
        this.actualVolume = 0;
    };
};
