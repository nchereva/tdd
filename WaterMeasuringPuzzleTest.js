describe('WaterMeasuringPuzzle', function() {
    describe('#checkFill', function() {
        it('check level of liquid in three gallon tube', function() {
            var puzzle = new Puzzle();
            result = puzzle.tubes.threeGallon;
            result.fill();
            result['actualVolume'].should.equal(7, 'dgfdsgfdsgds');
        });
    });
    describe('#checkLimit', function() {
        it('check if we reached correct level of liquid in any tube', function() {
            var puzzle = new Puzzle();
            result = puzzle.tubes;
            result['threeGallon'].volume.should.equal(3);
            result['fourGallon'].volume.should.equal(4);
        });
    });
    describe('#checkInitialFill', function() {
        it('check level of liquid of filled tube', function() {
            var puzzle = new Puzzle();
            var tubes = puzzle.tubes;
            tubes['threeGallon'].fill();
            var result = tubes['threeGallon'].isEmpty();
            result.should.equal(false);
        });
    });
    describe('#checkIsEmpty+Pour+Fill', function() {
        it('check level of liquid after basic actions on tubes', function() {
            var puzzle = new Puzzle();
            var tubes = puzzle.tubes;
            tubes['threeGallon'].fill();
            tubes['threeGallon'].pour();
            var result = tubes['threeGallon'].isEmpty();
            result.should.equal(true);
        });
    });
    describe('#checkTestScenario', function() {
        it('check test scenario for finding 2 gallond of liquid in any tube', function() {
            var puzzle = new Puzzle();
            var tubes = puzzle.tubes;
            puzzle.limit = 2;
            tubes.threeGallon.fill();
            puzzle.mix('threeGallon', 'fourGallon');
            tubes.threeGallon.fill();
            puzzle.mix('threeGallon', 'fourGallon');
            result = puzzle.checkLimit();
            result.should.equal(true);
        });
    });

});
